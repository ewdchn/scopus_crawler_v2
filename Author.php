<?php

    /**
     * Class Author
     */
class Author implements JsonSerializable{
    public static $author_list = array();

    public $id;
    public $name;
    public $affiliation;
    public $doc_count=null;
    public $citation_count=null;

    public static function getAuthor($_id){
        if(!isset(self::$author_list[$_id]))
            return false;
        return self::$author_list[$_id];
    }

    function __construct($_id,$_name,$_affiliation=null,$_doc_count=0,$_citation_count=0){
        $this->id=$_id;
        $this->name=$_name;
        $this->affiliation=$_affiliation;
        $this->doc_count=$_doc_count;
        $this->citation_count=$_citation_count;
        self::$author_list[$_id]=$this;
        //var_dump(array($this->id,$this->name,$this->affiliation));
    }

    public function jsonSerialize(){
        return get_object_vars($this);
    }
    public function setDocCount($_cnt){
        $this->doc_count=$_cnt;
    }
    public function setCitationCount($_cnt){
        $this->citation_count=$_cnt;
    }
} 
