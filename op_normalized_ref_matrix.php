<?php
    /**
     * Created by PhpStorm.
     * User: ewdchn
     * Date: 6/2/14
     * Time: 5:52 PM
     */
    require_once('main.php');
    $DB = 'scopus_II';
    writeEntriesToDb();
    writeEntryCoreferenceGraph(true);
    foreach (array('author', 'source', 'subject_area') as $attr) {
        writeCoreferenceGraph($attr,true);
    }
    system('python graphGenerator.py');
    system('python csvGenerator.py');