<?php

    /**
     * Class Source
     */
class Source implements JsonSerializable{

  public static $source_list = array();
  public $id;
  public $title;
  public $subject_area;
  public static function getSource($_id){
    if(!isset(self::$source_list[$_id]))
      return false;
    return self::$source_list[$_id];
  }
  function __construct($_id,Array $_arr){
    $this->id = $_id;
    $this->title = $_arr['title'];
    $this->subject_area=$_arr['subject_area'];
    self::$source_list[$_id] = $this;
  }
  function jsonSerialize(){
    return get_object_vars($this);
  }
}

