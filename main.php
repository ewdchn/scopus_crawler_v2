<?php
    /**
     * The main body of crawler
     */

    const SITEURL = "http://www.scopus.com";
    require_once('common/Helper.php');
    require_once('common/CitationGraph.php');
    require_once('common/Graph.php');
    require_once('db_op.php');
    require_once('Crawler.php');
    require_once('Parser.php');
    require_once('Entry.php');
    require_once('Author.php');
    require_once('Source.php');
    $IDX = 'eid';
    $ATTR = array('author', 'source', 'source_type', 'subject_area');

    /*
     *  wrapper, make sure all errors are caught and reported
     */
    function catchError($errno, $errstr, $errfile, $errline, array $errcontext) { throw new ErrorException($errstr, 0, $errno, $errfile, $errline); }


    /**
     * Given and eid of an entry
     *
     * Search the entry's citation and return their eids as an array
     *
     * @param $_eid
     * @return array
     */
    function getCitation($_eid)
    {
        $crawler   = new \Crawler\Crawler();
        $eid_array = array();
        list($result_cnt, $entry_lnk) = \Parser\parseTopicSearchPage($crawler->refSearch($_eid));
        for ($entry_cnt = 1; $entry_cnt <= $result_cnt; $entry_cnt++) {
            echo '/';
            parse_str(parse_url(html_entity_decode($entry_lnk), PHP_URL_QUERY), $url_attr);
            $eid_array[] = $url_attr['eid'];
            $entry_lnk   = \Parser\parseNextLinkOnly($crawler->getEntryPage($entry_lnk));
        }
        return $eid_array;
    }

    function crawlAuthor($author_id, $author_name, $affiliation)
    {
        $crawler = new \Crawler\Crawler();
        list($doc_count,$cit_count) = \Parser\parseDocCountCitCount($crawler->getAuthorPage($author_id));
        new Author($author_id, $author_name, $affiliation, $doc_count, $cit_count);
//        echo $doc_count." ".$cit_count."\n";
    }

    /**
     * Crawling logic of referenceSearch / citationSearch:
     *
     *   1. Perform a POST query (Digital Humanity OR humanity computing), get the result count and the first link to entry
     *
     *   2. Open the first entry page of result,
     *
     *   3. parse the content and author
     *
     *   4. If the entry page links to it's citations, crawl and parse them
     *
     *   5. Every entry page conatains a link to the next entry in result, we use it to travese through all results, repeat 2
     *
     * @return array eids of search results
     */


    function referenceSearch()
    {
        $result_entries = array();
        $crawler        = new \Crawler\Crawler();
        list($result_cnt, $entry_lnk) = \Parser\parseTopicSearchPage($crawler->topicSearch());
        for ($entry_cnt = 1; $entry_cnt <= $result_cnt; $entry_cnt++) {
            if (!$entry_lnk) break;
            $response = $crawler->getEntryPage($entry_lnk . '&fref=y');
            echo '.';
            //=======================PARSE ENTRY PAGE===============================//
            $a         = \Parser\parseEntryPage($response);
            $entry_lnk = isset($a['next_entry_url']) ? $a['next_entry_url'] : false;
            $entry     = new Entry($a);
            //=======================GET ENTRY REFERENCES============================//
            foreach (\Parser\parseRef($crawler->xhrReference($entry->attr['eid'])) as $ref) {
                if (!$ref['link']) continue;
                $a = \Parser\parseEntryPage($crawler->getEntryPage($ref['link']));
                new Entry($a);
                $entry->pushReference($a['eid']);
            }
            $result_entries[] = $entry->attr['eid'];
            //=======================PROCESS SOURCE==================================//
            if ($a['source'] && !Source::getSource($a['source'])) {
                if (is_numeric($a['source'])) {
                    $s = new Source($a['source'], \Parser\parseSourcePage($crawler->getSourcePage($a['source'])));
                } else {
                    $s = new Source($a['source'], array(
                        'id'           => $a['source'],
                        'title'        => $a['source'],
                        'subject_area' => array()));
                }
                //echo json_encode($s);
            }
            //=======================PROCESS AUTHORS=================================//
            foreach ($a['author'] as $author_id => $author_name) {
                if (!Author::getAuthor($author_id, $author_id)) {
                    crawlAuthor($author_id, $author_name, $a['affiliation']);
                }
            }
        }
        return $result_entries;
    }

    function citationSearch()
    {
        $result_entries = array();
        $reconnect_cnt  = 0;
        global $LOGFILE_NO;
        $crawler = new \Crawler\Crawler();
        // 1.
        list($result_cnt, $entry_lnk) = \Parser\parseTopicSearchPage($crawler->topicSearch());
        for ($entry_cnt = 1; $entry_cnt <= $result_cnt; $entry_cnt++) {
            if (!$entry_lnk) break;
            // 2.
            $response = $crawler->getEntryPage($entry_lnk);
            echo '.';
            // 3.
            //=======================PARSE ENTRY PAGE===============================//
            $a = \Parser\parseEntryPage($response);
            // 5.
            $entry_lnk        = isset($a['next_entry_url']) ? $a['next_entry_url'] : false;
            $entry            = new Entry($a);
            $result_entries[] = $a['eid'];
            //=======================PROCESS SOURCE==================================//
            if ($a['source'] && !Source::getSource($a['source'])) {
                if (is_numeric($a['source'])) {
                    $s = new Source($a['source'], \Parser\parseSourcePage($crawler->getSourcePage($a['source'])));
                } else {
                    $s = new Source($a['source'], array(
                        'id'           => $a['source'],
                        'title'        => $a['source'],
                        'subject_area' => array()));
                }
                //echo json_encode($s);
            }
            //=======================PROCESS AUTHORS=================================//
            foreach ($a['author'] as $author_id => $author_name) {
                if (!Author::getAuthor($author_id, $author_id)) {
                    crawlAuthor($author_id, $author_name, $a['affiliation']);
                }
            }
            // 4.
            $entry->addCitation(getCitation($entry->attr['eid']));
        }
        return $result_entries;
    }

