<?php

    /**
     * Class Entry
     */
class Entry implements JsonSerializable
{
    public static $entry_list = array();

    public $attr = array(
        'eid'=>null,
        'title'=>null,
        'DOI'=>null,
        'ISSN'=>null,
        'source'=>null,
        'abstract'=>null,
        'document_type'=>null,
        'source_type'=>null,
        'author_keyword'=>array(),
        'author'=>array(),
        'citation'=>array(),
        'reference'=>array(),
        'volume_info'=>null,
    );
    function __construct($a){
        foreach($this->attr as $key=>$value){
            $this->attr[$key]=$a[$key];
        }
        if($this->checkIntergrity()){
            self::$entry_list[$this->attr['eid']]=$this;
        }
        else{
            throw new \Exception('intergrity check failed');
        }
    }
    public function jsonSerialize(){
        return $this->attr;
    }

    public function checkIntergrity(){
        return true;
    }

    public function pushReference($_eid){
        $this->attr['reference'][] = $_eid;
    }
    public function addCitation($_eid_array){
        if(is_array($_eid_array)){
            $this->attr['citation']=$_eid_array;
        }
    }

} 