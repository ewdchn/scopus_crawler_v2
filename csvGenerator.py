# -*- coding: utf-8 -*-
import sys
import os
import MySQLdb
import MySQLdb.cursors
import csv
import passwd

def genEntryCsv():
    with open('entry.csv','wb') as csv_file:
        #initiation
        con = MySQLdb.connect("localhost",passwd.USER,passwd.PASSWD,"scopus_II",cursorclass=MySQLdb.cursors.DictCursor)
        writer = csv.writer(csv_file,dialect=csv.excel,quoting=csv.QUOTE_ALL)
        cur = con.cursor()
        #columns
        cur.execute('DESCRIBE entry;')
        column_key_rows = cur.fetchall()
        entry_attrs = [x['Field'] for x in column_key_rows]
        #the following attributes has their own index(ID), need dedicated processing
        indexed_extra_attrs = ['author','source']
        #while these don't any
        unindexed_extra_attrs = ['subject_area','author_keyword']
        extra_attrs = indexed_extra_attrs+unindexed_extra_attrs
        all_attrs = entry_attrs+extra_attrs
        writer.writerow(all_attrs)
        cur.execute('SELECT * FROM {0} e  WHERE e.eid IN (SELECT DISTINCT eid_I FROM {1})'.format('entry','entry_coreference'))
        for row in cur.fetchall():
            v = []
            for key in entry_attrs:
                v.append(row[key])
            #author
            cur.execute('SELECT {0} FROM {0} a WHERE a.eid={1}'.format('author',"'"+row['eid']+"'"))
            v.append(' '.join(['"'+x['author']+'"' for x in cur.fetchall()]))
            #source
            cur.execute('SELECT title FROM source s,source_title a WHERE s.source=a.source AND s.eid={0}'.format("'"+row['eid']+"'"))
            v.append(' '.join(['"'+x['title']+'"' for x in cur.fetchall()]))
            for key in unindexed_extra_attrs:
                cur.execute('SELECT {0} FROM {0} a WHERE a.eid={1}'.format(key,"'"+row['eid']+"'"))
                v.append(' '.join(['"'+x[key]+'"' for x in cur.fetchall()]))
            writer.writerow(v)

def genAllKeywords():
    with open('author_keywords.csv','wb') as csv_file:
        #initiation
        con = MySQLdb.connect("localhost",passwd.USER,passwd.PASSWD,"scopus_II",cursorclass=MySQLdb.cursors.DictCursor)
        writer = csv.writer(csv_file,dialect=csv.excel,quoting=csv.QUOTE_ALL)
        cur = con.cursor()
        cur.execute('SELECT DISTINCT a.author_keyword FROM author_keyword a,result_entry e WHERE a.eid=e.eid')
        writer.writerow(["author_keywords"])
        for row in cur.fetchall():
          writer.writerow([row['author_keyword']])



def genAuthorCsv():
    with open('authors.csv','wb') as csv_file:
        con = MySQLdb.connect("localhost",passwd.USER,passwd.PASSWD,"scopus",cursorclass=MySQLdb.cursors.DictCursor)
        cur = con.cursor()
        cur.execute('DESCRIBE author_detail;')
        column_key_rows = cur.fetchall()
        writer = csv.writer(csv_file,dialect=csv.excel,quoting=csv.QUOTE_ALL)
        entry_attrs = [x['Field'] for x in column_key_rows]
        writer.writerow(entry_attrs)
        cur.execute('SELECT * FROM author_detail a WHERE a.author IN (SELECT author_I from author_cocitation);')
        for row in cur.fetchall():
            v=[]
            for key in entry_attrs:
                v.append(row[key])
            writer.writerow(v)


genEntryCsv()
