<?php
    require_once('db_access.php');
    /**
     * db_access.php is ignore in git for security
     * the file should contain the following line
     * $USER=""
     * $PASS=""
     */
    $DBH = null;
    function init_db()
    {
        global $DB, $DBH, $USER, $PASS;
        if ($DBH) return $DBH;
        try {
            $DBH = new PDO("mysql:dbname={$DB};host=localhost", $USER, $PASS);
        } catch (PDOException $e) {
            die("Error!: " . $e->getMessage());
        }
        return $DBH;
    }

    function parseDate($_d)
    {
        if (!$_d) {
            return null;
        }
        foreach (explode(',', $_d) as $d) {

            if (preg_match('/(Issue)|(Page)|(Volume)|(Article)/', $d, $m)) {
                continue;
            }
            if (preg_match('/[0-9]{4}/', $d, $m)) {
                return $m[0];
            } else {
                return date_parse($d)['year'];
            }
        }
        return null;
    }

    /**
     * Wrapper for executing PDO Statements
     *
     * Makes sure that the statement is executed correctly else throws error
     *
     * @param PDOStatement $_op
     * @param array $_args
     * @return PDOStatement
     * @throws Exception
     */
    function execDbStatement(PDOStatement $_op, $_args = array())
    {
        if (!$_op->execute($_args)) {
            throw new \Exception('db statement execute error: ' . $_op->queryString);
        }
        return $_op;
    }

    /**
     * Read the entry json file and write the entries/citation/reference data to database
     *
     */
    function writeEntriesToDb()
    {
        echo "writing entries.....";
        //json files
        $ALL_ENTRIES    = json_decode(file_get_contents('entries.json'), true);
        $RESULT_ENTREIS = json_decode(file_get_contents('result_entries.json'), true);
        echo sizeof($ALL_ENTRIES) . " entries\n";

        //-----------------------------db op handler-----------------------//
        $dbh          = init_db();
        $insert_entry = $dbh->prepare('INSERT INTO entry (eid,title,DOI,ISSN,abstract,document_type,published)VALUES (?,?,?,?,?,?,?);');
        $insert_attr  = array(
            "author"         => $dbh->prepare('INSERT INTO author(eid,author)VALUES(?,?);'),
            "source"         => $dbh->prepare('INSERT INTO source(eid,source)VALUES(?,?);'),
            "author_keyword" => $dbh->prepare('INSERT INTO author_keyword(eid,author_keyword)VALUES(?,?);'),
            "source_type"    => $dbh->prepare('INSERT INTO source_type(eid,source_type) VALUES(?,?);'),
            "citation"       => $dbh->prepare('INSERT INTO citation(eid_I,eid_II) VALUES(?,?);'),
            "reference"      => $dbh->prepare('INSERT INTO reference(eid_I,eid_II)VALUES(?,?)'),
        );
        //------------------------truncate tables------------------------//
        $dbh->exec('TRUNCATE TABLE entry;');
        $dbh->exec('TRUNCATE TABLE result_entry');
        foreach ($insert_attr as $key => $value) {
            $dbh->exec('TRUNCATE TABLE ' . $key . ';');
        }

        //----------------prepare insert transactions--------------------//
        $dbh->beginTransaction();
        foreach ($RESULT_ENTREIS as $entry) {
//            echo $entry;
            $dbh->exec('INSERT INTO result_entry(eid) VALUES("' . $entry . '");');

        }

        foreach ($ALL_ENTRIES as $entry) {
            execDbStatement($insert_entry, array($entry['eid'], $entry['title'], $entry['DOI'], $entry['ISSN'], $entry['abstract'], $entry['document_type'], parseDate($entry['volume_info'])));
            foreach ($insert_attr as $key => $op) {
                if ($key == 'author') {
                    foreach ($entry[$key] as $author_id => $author_attr) {
                        execDbStatement($op, array($entry['eid'], $author_id));
                    }
                } else if (isset($entry[$key]) && !empty($entry[$key])) {
                    if (is_array($entry[$key])) {
                        foreach ($entry[$key] as $value) {
                            execDbStatement($op, array($entry['eid'], $value));
                        }
                    } else {
                        execDbStatement($op, array($entry['eid'], $entry[$key]));
                    }
                }
            }
        }
        $dbh->commit();
        unset($dbh);
        echo "done\n";
    }

    /**
     * Authors have their own detailed information
     *
     * (name, affiliation, document count, citation count)
     *
     * A secondary table is used to store them
     */
    function writeAuthorsToDb($_author_array = null)
    {
        $dbh = init_db();
        $dbh->exec('TRUNCATE TABLE author_detail;');
        $dbh->beginTransaction();
        $AUTHORS              = $_author_array ? $_author_array : json_decode(file_get_contents('author.json'), true);
        $insert_author_detail = $dbh->prepare('INSERT INTO author_detail(author,name,affiliation,doc_count,citation_count)VALUE(?,?,?,?,?)');
        foreach ((array)$AUTHORS as $a) {
            $author = (array)$a;
            execDbStatement($insert_author_detail, array($author['id'], $author['name'], preg_replace('/<sup>.*<\/sup>/', '', $a['affiliation']), intval($author['doc_count']), intval($author['citation_count'])));
        }
        $dbh->commit();
        unset($dbh);
    }


    /**
     * like writeAuthorsToDb
     *
     * Might never get used
     *
     *
     * @param null $_source_array
     *
     */
    function writeSourcesToDb($_source_array = null)
    {
        $_source_array = is_null($_source_array) ? json_decode(file_get_contents('source.json')) : $_source_array;
        $dbh           = init_db();
        $dbh->exec("TRUNCATE TABLE source_title");
        $dbh->exec('TRUNCATE TABLE source_subject_area');
        $dbh->beginTransaction();
        $insert_title        = $dbh->prepare('INSERT INTO source_title(source,title)VALUES(?,?);');
        $insert_subject_area = $dbh->prepare('INSERT INTO source_subject_area(source,subject_area)VALUES(?,?);');
        foreach ((array)$_source_array as $s) {
            $insert_title->execute();
            execDbStatement($insert_title, array($s->id, $s->title));
            foreach ($s->subject_area as $a) {
                execDbStatement($insert_subject_area, array($s->id, $a));
            }
        }

        $dbh->commit();
        unset($dbh);
    }



    function getAuthorsInGraph()
    {
        $dbh    = init_db();
        $result = $dbh->query('SELECT DISTINCT author_I FROM author_cocitation');
        return array_column($result->fetchAll(), 'author_I');
    }

    function getEntrySubjectArea()
    {
        $dbh = init_db();
        $dbh->exec('TRUNCATE TABLE subject_area;');
        $dbh->exec('INSERT INTO subject_area(eid,subject_area) SELECT e.eid,a.subject_area FROM entry e,source s,source_subject_area a WHERE e.eid=s.eid AND s.source=a.source;');
    }

?>
