<?php
namespace Crawler;

require_once('form_data.php');


/**
 * Class Crawler
 * Manages session between the website
 * An instance of crawler represents a session
 */
class Crawler
{
    public static $session_cnt = 0;
    public $session_no;
    public $cookie_storage;


    function __construct()
    {
        try {
            checkOrCreateDir("cookies");
            $this->session_no     = self::$session_cnt;
            $this->cookie_storage = "cookies/cookie_" . $this->session_no . ".txt";
            file_put_contents($this->cookie_storage, "");
            self::$session_cnt++;
            $main_page = initSession($this->cookie_storage);

        } catch (Exception $e) {
            print $e->getMessage();
            exit(0);
        }
    }

    function __destruct()
    {
        file_put_contents($this->cookie_storage, "");
    }

    /**
     * Perform a search on the website (Digital Humanity or humanity computing)
     *
     * @return array|bool
     */
    public function topicSearch()
    {
        $options                     = array();
        $options[CURLOPT_URL]        = 'http://www.scopus.com/search/submit/basic.url';
        $options[CURLOPT_POST]       = true;
        $options[CURLOPT_POSTFIELDS] = $GLOBALS['form_data_str'];
        try {
            $response = getPage($options, $this->cookie_storage);
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $response;
    }

    /**
     * Search references of a entry given eid
     * @param $_eid
     * @return array|bool
     */
    public function refSearch($_eid){
        $options                     = array();
        $options[CURLOPT_URL]        = 'http://www.scopus.com/search/submit/citedby.url?eid='.$_eid.'&src=s&origin=recordpage';
        try {
            $response = getPage($options, $this->cookie_storage);
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $response;
    }


    /**
     *
     * @param $_author_id
     * @return mixed
     */
    public function getAuthorPage($_author_id){
        $options              = array();
        $options[CURLOPT_URL] = 'http://www.scopus.com/authid/detail.url?origin=resultslist&authorId='.$_author_id;
        try {
            $response      = getPage($options, $this->cookie_storage);
            $html_response = end(seperateHeader($response[0], $response[2]));
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $html_response;

    }


    public function getSourcePage($_source_id){
        $options              = array();
        $options[CURLOPT_URL] = 'http://www.scopus.com/source/sourceInfo.url?origin=recordpage&sourceId='.$_source_id;
        try {
            $response      = getPage($options, $this->cookie_storage);
            $html_response = end(seperateHeader($response[0], $response[2]));
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $html_response;

    }
    /**
     * @param $_link
     * @return mixed
     */
    public function getEntryPage($_link){
        $options              = array();
        $options[CURLOPT_URL] = $_link;
        try {
            $response      = getPage($options, $this->cookie_storage);
            $html_response = end(seperateHeader($response[0], $response[2]));
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $html_response;
    }

    //===========================XHR QUERIES=========================================//
    //The behavior of scopus website changes over time, some of the following might be deprecated

    /**
     * POSSIBLY DEPRECATED
     * @param $_author_id
     * @return mixed
     */
    public function xhrAuthorCitCount($_author_id){
        $options = array();
        $options[CURLOPT_HTTPHEADER]= array("X-Requested-With: XMLHttpRequest");
        $options[CURLOPT_REFERER] = 'http://www.scopus.com/authid/detail.url?origin=resultslist&authorId='.$_author_id;
        $options[CURLOPT_URL] = 'http://www.scopus.com/author/CtoCount.url';
        try {
            $response      = getPage($options, $this->cookie_storage);
            $html_response = end(seperateHeader($response[0], $response[2]));
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $html_response;
    }

    /**
     * POSSIBLY DEPRECATED
     * @param $_author_id
     * @return mixed
     */
    public function xhrAuthorDocCount($_author_id){
        $options = array();
        $options[CURLOPT_HTTPHEADER]= array("X-Requested-With: XMLHttpRequest");
        $options[CURLOPT_REFERER] = 'http://www.scopus.com/authid/detail.url?origin=resultslist&authorId='.$_author_id;
        $options[CURLOPT_URL] = 'http://www.scopus.com/author/documents.url?aid='.$_author_id.'&sgroup=';
        try {
            $response      = getPage($options, $this->cookie_storage);
            $html_response = end(seperateHeader($response[0], $response[2]));
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $html_response;
    }


    public function xhrReference($_eid){
        //http://www.scopus.com/record/references.url?origin=recordpage&currentRecordPageEID=2-s2.0-84894807450
        $options = array();
        $options[CURLOPT_HTTPHEADER]= array("X-Requested-With: XMLHttpRequest");
        $options[CURLOPT_REFERER] = 'http://www.scopus.com/record/display.url?origin=resultslist&eid='.$_eid;
        $options[CURLOPT_URL] = 'http://www.scopus.com/record/references.url?origin=recordpage&currentRecordPageEID='.$_eid;
        try {
            $response      = getPage($options, $this->cookie_storage);
//            echo $response[1]."  ";
//            var_dump($response);
            $html_response = end(seperateHeader($response[0], $response[2]));
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
            exit(0);
        }
        return $html_response;
    }
}
