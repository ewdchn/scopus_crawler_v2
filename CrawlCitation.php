<?php
require_once("main.php");
$result_entries = citationSearch();
file_put_contents('result_entries.json', json_encode($result_entries));
file_put_contents('entries.json', json_encode(Entry::$entry_list));
file_put_contents('author.json', json_encode(Author::$author_list));
file_put_contents('source.json', json_encode(Source::$source_list));
