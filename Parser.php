<?php
    namespace Parser;

    use simple_html_dom\simple_html_dom;

    require_once('simple_html_dom.php');
    require_once('Author.php');

//==========================utility functions====================================//
    function split_http_header_attritube($_header_str)
    {
        $header_attr = array();
        if (is_array($_header_str))
            $_header_str = array_shift($_header_str);
        $header_lines = explode("\r\n", $_header_str); //STATUS CODE
        array_shift($header_lines);
        foreach ($header_lines as $line) {
            $tmp_arr                  = explode(": ", $line, 2);
            $header_attr[$tmp_arr[0]] = $tmp_arr[1];
        }
        return $header_attr;
    }


    function getTextBlocks($_node)
    {
        $ans = array();
        foreach ($_node->find('text') as $text) {
            $ans[] = $text->plaintext;
        }
        return $ans;
    }

//=============================page parse functions===============================//
    /**
     * Returns (0,'') if page is not in right format
     *
     * return format: array(result_cnt,link to first entry)
     *
     * @param $_response
     * @return array
     *
     */
    function parseTopicSearchPage($_response)
    {
        $html = array_pop(seperateHeader($_response[0], $_response[2]));
        $dom = new \simple_html_dom\simple_html_dom($html);
        if (!$dom->find('span.resultsCount', 0))
            return array(0, '');
        $result_cnt = intval(str_replace(',','',$dom->find('span.resultsCount', 0)->innertext));
        $first_article_lnk = $dom->find('span.docTitle a', 0)->href;
        return array($result_cnt, html_entity_decode($first_article_lnk));
    }

    /**
     * @param $_html
     * @return bool|string
     */
    function parseNextLinkOnly($_html)
    {
        $dom = new simple_html_dom($_html);
        if ($span = $dom->find('span[title="Next Page for the Next Record"]', 0)) {
            return html_entity_decode($span->parent()->href);
        }
        return false;
        unset($dom);
    }

    /**
     * @param $_html
     * @return array
     */
    function parseAuthorPage($_html)
    {
        $dom            = new simple_html_dom($_html);
        $work_count     = 0;
        $citation_count = 0;

        unset($dom);
        return array($work_count, $citation_count);
    }

    function parseSourcePage($_html)
    {
        $dom                 = new simple_html_dom($_html);
        $ans['title']        = $dom->find('.txtTitle', 0)->innertext;
        $ans['subject_area'] = getTextBlocks($dom->find('.sourceInfoRow .floatL', 0));
        foreach ($ans['subject_area'] as $key => $sub) {
            $ans['subject_area'][$key] = trim($sub, ' ');
        }
        unset($dom);
        return $ans;
    }

    /**
     * **POSSIBLY DEPRECATED DUE TO SCOPUS SITE BEHAVIOR CHANGE
     * **SEE parseDocCountCitCount
     * parse document count of a author
     *
     * @param $_html
     * @return int
     */
    function parseDocCount($_html)
    {
        $dom       = new simple_html_dom($_html);
        $doc_count = 0;
        $doc_count = intval($dom->find('input#doccount', 0)->value);
        unset($dom);
        return $doc_count;
    }


    /**
     * **POSSIBLY DEPRECATED DUE TO SCOPUS SITE BEHAVIOR CHANGE
     * **SEE parseDocCountCitCount
     * parse citation count of a author
     *
     * @param $_html
     * @return int
     */
    function parseCitCount($_html)
    {
        $cit_count  = 0;
        $components = explode('#', $_html);
        $cit_count  = intval($components[0]);
        return $cit_count;
    }

    function parseDocCountCitCount($_html){
        $dom       = new simple_html_dom($_html);
        $doc_count = $dom->find('#doccountVal',0)->value;
        $cit_count = $dom->find('#citecountVal',0)->value;
        unset($dom);
        return [$doc_count,$cit_count];
    }

    /**
     *
     * @param $_html
     * @return array
     */
    function parseEntryPage($_html)
    {
        $dom       = new \simple_html_dom\simple_html_dom($_html);
        $attr_keys = array(
            'DOI'           => 'DOI',
            'ISSN'          => 'ISSN',
            'document_type' => 'Document Type',
            'source_type'   => 'Source Type',
        );
        $ans       = array(
            'eid'            => null,
            'title'          => null,
            'source'         => null,
            'abstract'       => null,
            //================//
            'DOI'            => null,
            'ISSN'           => null,
            'document_type'  => null,
            'source_type'    => null,
            //====================//
            'author_keyword' => array(),
            'author'         => array(),
            'citation'       => array(),
            'reference'      => array(),
            'volume_info'    => null,
            //====================//
            'affiliation'    => null,
        );
        //=====================EID & TITLE============================//
        if($dom->find('a[title="Set citation alert"]'))
            parse_str(parse_url(html_entity_decode($dom->find('a[title="Set citation alert"]', 0)->href), PHP_URL_QUERY), $url_attr);
        else
            parse_str(parse_url(html_entity_decode($dom->find('a.addToMyList', 0)->href), PHP_URL_QUERY), $url_attr);

        $ans['eid'] = $url_attr['eid'];
        $title_node = $dom->find('.txtTitle ', 0);
        if ($s = $title_node->find('span.documentType', 0)) {
            $s->outertext = "";
        }
        $ans['title'] = $title_node->plaintext;
        //=======================VOLUMNE INFO=========================//
        if ($span = $dom->find('.volumeInfo', 0)) {
            $ans['volume_info'] = $span->innertext;
        }
        //=======================AFFLIATION===========================//
        if ($span = $dom->find('#affiliationlist span', 0)) {
            $ans['affiliation'] = $span->innertext;
        }
        //=======================ABSTRACT=========================//
        if ($h2 = $dom->find('h2[class="subTitle paddingT7 paddingB10"]', 0)) {
            $ans['abstract'] = $h2->next_sibling()->plaintext;
        }
        //=======================AUTHOR KEYWORDS=====================//
        if ($h2 = $dom->find('h2[class="subTitle txtLarger paddingT2 paddingB10"]', 0)) {
            $ans['author_keyword'] = explode('; ', $h2->next_sibling()->plaintext);
        }

        //=======================SOURCE=============================//
        if ($a = $dom->find('a[title="Go to the information page for this source"]', 0)) {
            parse_str(parse_url(html_entity_decode($a->href), PHP_URL_QUERY), $url_attr);
            $ans['source'] = $url_attr['sourceId'];
        } else if ($span = $dom->find('.sourceTitle', 0)) {
            $ans['source'] = $span->plaintext;
        }
        //=======================AUTHORS============================//
        foreach ($dom->find('#authorlist span') as $span) {
            if ($a = $span->find('a[title="Show Author Details"]', 0)) {
                parse_str(parse_url(html_entity_decode($a->href), PHP_URL_QUERY), $url_attr);
                $ans['author'][$url_attr['authorId']] = $a->innertext;
            }
        }

        //==============other attributes======================//
        foreach ($dom->find('span.paddingR15') as $span) {
            $txt_blocks = getTextBlocks($span);
            $value      = array_pop($txt_blocks);
            $name       = array_pop($txt_blocks);
            foreach ($attr_keys as $k => $a) {
                if (strpos($name, $a) !== false) {
                    $ans[$k] = $value;
                    break;
                }
            }
        }

        //================END OF ATTRIBUTE PARSING=====================//
        if ($nextlink = $dom->find('span[title="Next Page for the Next Record"]', 0))
            $ans['next_entry_url'] = html_entity_decode($nextlink->parent()->href);

        unset($dom);
        return $ans;
    }

    /**
     * return array of eids an entry references
     *
     * @param $_html
     * @return array
     */
    function parseRef($_html)
    {
        $ans = array();
        $dom = new simple_html_dom($_html);
        foreach ($dom->find('.referencesBlk') as $blk) {
            if ($lnk = $blk->find('a[title^="View the"]', 0)) {
                parse_str(parse_url(html_entity_decode($lnk->href), PHP_URL_QUERY), $url_attr);
                $ans[] = array('link' => 'http://www.scopus.com/record/display.url?origin=resultslist&eid=' . $url_attr['refeid']);
            }
        }
        unset($dom);
        return $ans;
    }

