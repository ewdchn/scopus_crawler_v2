<?php
    require_once('main.php');
    $DB = 'scopus_II';
    writeEntriesToDb();
    writeEntryCoreferenceGraph();
    foreach (array('author', 'source', 'subject_area') as $attr) {
        writeCoreferenceGraph($attr);
    }
    system('python graphGenerator.py');
    system('python csvGenerator.py');
