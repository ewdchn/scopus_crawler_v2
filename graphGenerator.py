# -*- coding: utf-8 -*-
import sys
import os
import MySQLdb as mdb
import passwd
prefix = ['author','entry','subject_area','source']
if len(sys.argv)<2:
    suffix = 'reference'
else:
    suffix = sys.argv[1]
#tables = ['author_coreference']
debug=False
con = mdb.connect("localhost",passwd.USER,passwd.PASSWD,"scopus_II")
for p in prefix:
    table = p+'_co'+suffix
    if p == 'entry':
        p = 'eid'
    print table
    cur = con.cursor()
    #node count
    cur.execute('SELECT COUNT(*) FROM (SELECT {0}_I FROM {1} UNION SELECT {0}_II FROM {1})x'.format(p,table))
    node_count = cur.fetchone()[0]
    cur.execute('SELECT * FROM {0}'.format(table))
    rows = cur.fetchall()
    output = []
    output.append('dl n = {0} format = edgelist1'.format(node_count))
    output.append('labels embedded')
    output.append('data:')
    for row in rows:
        if debug:
            if p=='eid':
                cur.execute('SELECT COUNT(*) FROM {0} WHERE {1}=\'{2}\''.format(suffix,'eid_I',mdb.escape_string(row[0])))
            else:
                cur.execute('SELECT COUNT(*) FROM {0} a, {1} r WHERE a.{2}=r.{2}_I AND a.{0}=\'{3}\''.format(p,suffix,'eid',mdb.escape_string(row[0])))
                count_I = cur.fetchone()[0]
                cur.execute('SELECT COUNT(*) FROM {0} a, {1} r WHERE a.{2}=r.{2}_I AND a.{0}=\'{3}\''.format(p,suffix,'eid',mdb.escape_string(row[1])))
                count_II = cur.fetchone()[0]
            output.append('{0} {1}'.format(count_I,count_II))
        output.append('"{0}" "{1}" {2:.6f}'.format(row[0],row[1],row[2]))
    output_file = file('{0}.dl'.format(table),'w+')
    output_file.writelines(map(lambda x:x+"\n",output))
