<?php
    require_once("main.php");
    $result_entries = referenceSearch();
    $dir            = date('m-d', time());
    file_put_contents('result_entries.json', json_encode($result_entries));
    file_put_contents('entries.json', json_encode(Entry::$entry_list));
    file_put_contents('author.json', json_encode(Author::$author_list));
    file_put_contents('source.json', json_encode(Source::$source_list));
    system("cp result_entries.json ${dir}");
    system("cp author.json ${dir}");
    system("cp source.json ${dir}");
    system("cp entries.json ${dir}");